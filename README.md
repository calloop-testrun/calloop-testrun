# Calloop Testrun

Testing utilities for the Calloop event loop.

> ## This is very much a draft!
>
> It is (currently) only published to verify CI and use as a non-local dependency for other crates' development.
> The entire API is in an extreme state of flux while usage is tested in dependent crates.
